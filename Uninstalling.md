**PER SITE COLLECTION**

These steps will uninstall the Tool from your site collection.

1.	Open **Site Settings** at the root of your site collection

2.	Select **Solutions** under **Web Designer Galleries**

3.	Select the* BTPlus*

4.	Open the item dropdown and choose the **Deactivate** option

5.	Press the **Deactivate** button on the dialog and refresh the page

6.	Open the item dropdown and press the **Delete** option

The tool is now fully removed from your site collection.
