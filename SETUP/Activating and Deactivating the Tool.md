**PER SITE COLLECTION**

These steps will activate or deactivate the Tool for a single site collection where it's already installed.

1.	Open **Site Settings** at the root of your site collection

2.	Select **Site Collection Features** under **Site Collection Administration**

3.	Look for the **BindTuning Plus Tool** feature from the list

4.	Press **Activate** or **Deactivate** button to toggle the tool ON or OFF

