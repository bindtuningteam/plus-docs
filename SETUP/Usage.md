Once activated, the icons for BindTuning lists, web parts and features should appear customized.

Feature icons

![Picture6.png](https://bitbucket.org/repo/Xqr9aB/images/4173366661-Picture6.png)

Web Part icons

![Picture7.png](https://bitbucket.org/repo/Xqr9aB/images/1796972229-Picture7.png)
